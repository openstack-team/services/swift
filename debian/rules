#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

export SWIFT_TEST_CONFIG_FILE=$(CURDIR)/debian/swift-test.conf

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc

# Control if proxy + account + container server should be using UWSGI or not
export DEB_USE_UWSGI=no

override_dh_auto_test:
	echo "Do nothing..."

override_dh_auto_clean:
	rm -rf build .stestr *.egg-info .pybuild
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_build:
	dh_auto_build

	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func python3-swift.postinst

	set -e ; set -x ; \
	for i in	debian/swift-account.swift-account-auditor.init.in \
			debian/swift-account.swift-account-reaper.init.in \
			debian/swift-account.swift-account-replicator.init.in \
			debian/swift-container.swift-container-auditor.init.in \
			debian/swift-container.swift-container-reconciler.init.in \
			debian/swift-container.swift-container-replicator.init.in \
			debian/swift-container.swift-container-sharder.init.in \
			debian/swift-container.swift-container-sync.init.in \
			debian/swift-container.swift-container-updater.init.in \
			debian/swift-object-expirer.swift-object-expirer.init.in \
			debian/swift-object.swift-object-auditor.init.in \
			debian/swift-object.swift-object.init.in \
			debian/swift-object.swift-object-reconstructor.init.in \
			debian/swift-object.swift-object-replicator.init.in \
			debian/swift-object.swift-object-updater.init.in \
			; do \
		debian/gen-init $$i ; \
	done

	set -e ; set -x ; \
	for i in	debian/swift-proxy.swift-proxy.init \
			debian/swift-account.swift-account.init \
			debian/swift-container.swift-container.init \
			; do \
		if [ $$DEB_USE_UWSGI = "yes" ] ; then \
			cp $$i.in $$i ; \
			cat /usr/share/openstack-pkg-tools/init-script-template >>$$i ; \
			pkgos-gen-systemd-unit $$i.in ; \
		else \
			CONFIG=`echo $$i | cut -d/ -f2 | cut -d. -f1 | sed -e s/swift-//`-server debian/gen-init $$i.in ; \
		fi ; \
	done

override_dh_auto_install:
	pkgos-dh_auto_install --no-py2 --in-tmp
	rm debian/tmp/usr/bin/swift-init
# https://review.opendev.org/c/openstack/swift/+/904601
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	pkgos-dh_auto_test --no-py2 --test-path test/unit 'test\.unit\.(?!common\.test_utils\.TestUtils\.test_get_logger_sysloghandler_plumbing|common\.middleware\.test_cname_lookup\.TestCNAMELookup.*|common\.test_db\.TestDatabaseBroker\.test_get.*|container\.test_sync\.TestContainerSync\.test_init.*|common\.test_utils\.TestPunchHoleReally\.test_punch_a_hole.*|common\.test_utils\.Test_LibcWrapper\.test_argument_plumbing.*|common\.test_utils\.TestUtils\.test_load_pkg_resource_importlib.*|common\.test_db_replicator\.TestHandoffsOnly.*|common\.ring\.test_builder\.TestGetRequiredOverload\.test_perfect_four_zone_four_replica_bad_placement|common\.test_wsgi\.TestWSGI.*|obj\.test_expirer\.TestObjectExpirer\.test_delete_actual_object|common\.test_memcached\.TestMemcached\.test_get_conns_hostname6|common\.test_memcached\.TestMemcached\.test_get_conns_v6|common\.test_memcached\.TestMemcached\.test_get_conns_v6_default|obj\.test_reconstructor\.TestWorkerReconstructor\.test_run_forever_recon_aggregation|proxy\.controllers\.test_obj\.TestECObjController\.test_GET_with_duplicate_but_sufficient_frag_indexes|proxy\.test_mem_server\.TestProxyServer\.test_statsd_prefix|proxy\.controllers\.test_obj\.TestECObjController\.test_GET_read_timeout|proxy\.controllers\.test_base\.TestGetOrHeadHandler\.test_disconnected_logging|obj\.test_expirer\.TestObjectExpirer\.test_failed_delete_continues_on|obj\.test_replicator\.TestObjectReplicator\.test_update|obj\.test_ssync_receiver\.TestReceiver\.test_UPDATES_timeout|obj\.test_ssync_receiver\.TestReceiver\.test_UPDATES_no_problems_no_hard_disconnect|common\.test_memcached\.TestMemcached\.test_operations_timing_stats_with_incr_timeout|obj\.test_expirer\.TestExpirerHelpers\.test_embed_expirer_bytes_from_diskfile_metadata|common\.middleware\.test_proxy_logging\.TestProxyLogging\.test_init_statsd_options_access_log_prefix|common\.middleware\.test_proxy_logging\.TestProxyLogging\.test_init_statsd_options_log_prefix)'
endif

override_dh_installinit:
	set -e ; set -x ; \
	for FILE in $(sort $(patsubst debian/%,%,$(wildcard debian/*.init))) ; do \
		PKG=`echo $$FILE | cut -d. -f1` ; \
		NAME=`echo $$FILE | cut -d. -f2` ; \
		dh_installinit --error-handler=true -p$$PKG --name=$$NAME ;\
	done

override_dh_installsystemd:
	set -e ; set -x ; \
	for FILE in $(sort $(patsubst debian/%,%,$(wildcard debian/*.service))) ; do \
		PKG=`echo $$FILE | cut -d. -f1` ; \
		NAME=`echo $$FILE | cut -d. -f2` ; \
		dh_installsystemd -p$$PKG --name=$$NAME ;\
	done

gen-init-configurations:
	echo "Do nothing"

# build with sphinx documentation
override_dh_sphinxdoc:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	LC_ALL=C.UTF-8 PYTHONPATH=. python3 -m sphinx doc/source $(CURDIR)/debian/swift-doc/usr/share/doc/swift-doc/base
	LC_ALL=C.UTF-8 PYTHONPATH=. python3 -m sphinx api-ref/source $(CURDIR)/debian/swift-doc/usr/share/doc/swift-doc/api-ref
	dh_sphinxdoc
endif

override_dh_installdocs:
	dh_installdocs

	dh_installdocs -A AUTHORS

override_dh_missing:
	dh_missing --fail-missing

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
